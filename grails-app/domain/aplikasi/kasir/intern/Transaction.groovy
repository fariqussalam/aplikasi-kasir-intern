package aplikasi.kasir.intern

class Transaction {
    String code
    Date dateTransaction
    BigDecimal totalPayment

    static hasMany = [detailTransactions : DetailTransaction]

    static constraints = {
    }
}
