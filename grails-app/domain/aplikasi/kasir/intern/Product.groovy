package aplikasi.kasir.intern

class Product {

    String code
    String name
    String thumbnailUrl
    Integer stock
    Integer price
    Date dateCreated
    String description

    static mapping = {
        description sqlType: "text"
    }

    static hasMany = [listProduct : DetailTransaction]

    static constraints = {
        description nullable: true
    }
}
