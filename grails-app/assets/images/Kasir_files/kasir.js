(function ($) {
    var code = [];
    var idList = [];
    var quantities = [];
    $('#myModal').on('show.bs.modal', function(e){
        var modal = $(this);
        var a = e.relatedTarget;
        var parent = a.closest('tr');
        var image = $(parent).find('.js-product-image').data("image");
        var productName = $(parent).find('.js-tbl-product-name').text();
        var price = $(parent).find('.js-tbl-product-price').text();

        var nilaiStr = $('.nilai').val();

        if (nilaiStr <= 1){
            $('.mines').attr('disabled', 'disabled');
        }

        $('.plus').off().on('click',function(){
            var nilaiStr = $('.nilai').val();
            var nilai = parseInt(nilaiStr);
            $('.nilai').val(nilai+1);
            var newNilaiStr = $('.nilai').val();
            var newNilai = parseInt(newNilaiStr);
            var total = newNilai * price;
            $('.total').text(total);
            $('.mines').removeAttr('disabled', 'disabled');
        });

        $('.mines').off().on('click',function(){
            var nilaiStr = $('.nilai').val();
            var nilai = parseInt(nilaiStr);
            $('.nilai').val(nilai-1);
            var newNilaiStr = $('.nilai').val();
            if (nilaiStr <= 2){
                $('.mines').attr('disabled', 'disabled');
            }

            var newNilai = parseInt(newNilaiStr);
            var total = newNilai * price;
            $('.total').text(total);
        });

        // $('.addProduct').off().on('click', function () {
        //     var product = $(modal).find('.js-product-name').text();
        //     var qwt     = parseInt($(modal).find('.nilai').val());
        //     var total   = $(modal).find('.total').text();
        //     var i=1;
        //     var template= "<tr>\n" +
        //         "                            <td>"+i+++"</td>\n" +
        //         "                            <td class=\"js-produk\">"+product+"</td>\n" +
        //         "                            <td value=\"\" class=\"js-qwt\">"+qwt+"</td>\n" +
        //         "                            <td class=\"js-price\">"+total+"</td>\n" +
        //         "                            <td><a href=\"\"><i><span class=\"fa fa-trash\"></span></i></a></td>\n" +
        //         "                        </tr>";
        //
        //     var a = $(".list-transaction").find(".js-produk").text();
        //     var b = parseInt($(".list-transaction").find(".js-qwt").text());
        //     var c = $(".list-transaction").find(".js-price").text();
        //     var f = parseInt(total.replace('Rp ', ""));
        //
        //     if (a == product) {
        //         var g = parseInt(c.replace('Rp ', ""));
        //         var d = b+qwt;
        //         var e = f+g;
        //
        //         modal.closest('.konten').find('.js-qwt').text(d);
        //         modal.closest('.konten').find('.js-price').text("Rp " +e);
        //
        //     } else {
        //
        //         $(".list-transaction").append(template);
        //         // modal.closest('.konten').find('.js-produk').text(product);
        //         // modal.closest('.konten').find('.js-qwt').text(qwt);
        //         // modal.closest('.konten').find('.js-price').text(total);
        //     }
        //
        //     var tot = e;
        //     modal.closest('.buttom').find(".totalHarga").text(tot);
        //
        //     console.log(tot);
        //
        //     //index
        //     var z = parseInt(stock);
        //     var stok = z - qwt;
        //     modal.closest('.konten').find('.js-tbl-product-stock').text(stok);
        //
        //     console.log(stok);
        //
        // });


        $('.productAdd').off().on('click',function(){
            var stock = parseInt($(parent).find('.js-tbl-product-stok').text());
            var idProduct = $(parent).find('.js-tbl-product-id').val();
            var product = $(modal).find('.js-product-name').text();
            var qwt     = parseInt($(modal).find('.nilai').val());
            var total   = $(modal).find('.total').text();
            var template = "<tr>\n" +
                "                            <td class=\"js-id-product\" name=\"id\" value=\"${params?.id}\"><a href='#'></a>"+idProduct+"</td>\n" +
                "                            <td class=\"js-produk\" id=\"name\" name=\"name\" value=\"${params.name}\">"+product+"</td>\n" +
                "                            <td value=\"\" class=\"js-qwt\" id=\"stock\" name=\"stock\" value=\"${params.stock}\">"+qwt+"</td>\n" +
                "                            <td> Rp  <span class=\"js-price\" id=\"price\" name=\"price\" value=\"${params.price}\">"+total+"</span></td>\n" +
                "                            <td><a href='#' ><i class=\"fa fa-trash js-delete-data\"></i></a></td>\n" +
                "                        </tr>";

            var id = 1;
            var newProduct = true;
            var data = {
                "id": idProduct,
                "stock" : qwt
            };

            code.push(data);
            idList.push(idProduct);
            quantities.push(qwt);
            console.log(code);
            var allIdProduct = $(".list-transaction").find(".js-id-product");

            allIdProduct.each(function(){
               if (id == $(this).data("id")){
                   newProduct = false;
                   return;
               }
            });
            $(".list-transaction").append(template);

            var totalPrice = $('.js-price');
            var total = 0;
            totalPrice.each(function () {
               var price = $(this).text();
               var number = price.replace('Rp',"");
               var totalPriceInt = parseInt(number);
               total += parseFloat(totalPriceInt);
            });
            $('.js-total-price').text(total);


            var a = stock - qwt;
            $(parent).find('.js-tbl-product-stok').text(a);


        });
        modal.find('.js-product-name').text(productName);
        modal.find('.js-product-price').text(price);
        modal.find('.js-product-image').attr("src", image);

    });

    // var b = $(parent).find('.js-tbl-product-stok').text();
    // // var a = modal.find('.js-tbl-product-stok').text(b);
    // // var a = $('.konten').find('.js-tbl-product-stok').text(b);
    //
    //
    // console.log(b);
    //
    //
    // var c = parseInt(b) - parseInt(qwt);
    // console.log(c);


    var a =



    $('.list-transaction').on('click', '.js-delete-data', function () {
        $(this).closest('tr').remove();
    });


    $('.js-save-data').on('click', function(){
        // var id      = $("[name='id']").text();
        // var name    = $("[name='name']").text();
        // var stock   = $("[name='stock']").text();
        // var price   = $("[name='price']").text();
        var list    = $('.list-transaction');
        // var totalPrice = $('.js-total-price');
        // var modalsuccess = $('#flipFlop');
        // // console.log(id + " " + name +"  " + stock + "  " + price);
        // console.log(idList);

        // var listProduk = [];
        // list.find('tr').each(function(){
        //    var idProduct = $(this).find('td.js-id-product').text();
        //    var qwt = $(this).find('td.js-qwt').text();
        //    listProduk.push({id: idProduct, qwt: qwt});
        // })

        var url = $(this).data('url');
        // var _redirectUrl = _redirectUrl;
        // console.log(_redirectUrl);

        _executeListAction(url, $.param({
            ids : idList,
            quantities : quantities
        }, true)) ;

        if ( list < 1 || list.length < 1) {
            alert("No Transaction List. Make New Transaction!");
        }
        else{
            alert("Transaction Success!");
        }
       //  var listProduct = [];
       // list.find('tr').each(function () {
       //     var idProduct = $(this).find('.td.js-id-product').text();
       //     var qwt = $(this).find('.td.js-qwt').text();
       //     listProduct.push({id : idProduct, qwt : qwt});
       //
       // })
        // var stock = $('.js-tbl-product-stok').data("value");
        // // var stock = $('.js-tbl-product-stok').val();
        // var stockInt = parseInt(stock);
        // var stockListInt = parseInt(stockList);
        // var newStock = stockInt - stockListInt;
        // $('.js-tbl-product-stok').text(newStock);
        window.location.reload();

        list.remove();
        totalPrice.remove();
    });

    var _executeListAction = function (url, params) {
        $.post(url, params, function () {
                location.reload();
        })
    }
})(jQuery);


