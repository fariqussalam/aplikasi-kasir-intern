// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require bootstrap
//= require kasir
//= require_self


if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#photo')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(180);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $( "#upload" ).change(function() {
            console.log("teshh")
            readURL(this);
        });

        $('.warn').on('click',function(){
            var warning = confirm("Are you sure you want to delete ?");
            if( warning == true ){
                return true;
            }
            else {
                alert("User does not want to delete!");
                return false;
            }
        });

        // function getConfirmation() {
        //     var warn = confirm("Are you sure you want to delete ?");
        //     if( warn ){
        //         alert("User wants to delete!");
        //         return true;
        //     }
        //     else{
        //         alert ("User does not want to delete!");
        //         return false;
        //     }
        // }


        $('.js-code-data').on('change keyup',function () {
            var code = $(this).val();
            var url = $(this).data('url');
            var message = $('#warningMessage');
            var button = $('.js-save-product').attr('disabled', true);
            var buttonRemove = $('.js-save-product').removeAttr('disabled', true);


            // var data = $(this).data('resource');
            // console.log(data)

            $.ajax({
                method: "POST",
                url: url,
                data: {code: code},
                success: function (result) {
                    console.log(result)
                    if (result == "true"){
                        message.text("Code is already exist");
                        button;
                    } else {
                        buttonRemove;
                        message.text("");
                    }
                }

            })
        })



    })(jQuery);
}
