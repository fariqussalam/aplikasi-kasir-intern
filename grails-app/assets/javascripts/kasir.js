(function ($) {
    var code = [];
    var idList = [];
    var quantities = [];
    $('#myModal').on('show.bs.modal', function(e){
        var modal = $(this);
        var a = e.relatedTarget;
        var parent = a.closest('tr');
        var image = $(parent).find('.js-product-image').data("image");
        var productName = $(parent).find('.js-tbl-product-name').text();
        var price = $(parent).find('.js-tbl-product-price').text();

        var val = 1;
        $('.nilai').val(val);

        $('.js-input-value').off().on('change',function () {
            var value = $(this).val();
            var total = value * price;
            $('.total').text(total);

            var maxStock = parseInt($(parent).find('.js-tbl-product-stok').text());
            if (value < 1) {
                alert("Value must be greater than 0");
                $('.nilai').val(1);
                var minPrice = 1 * price;
                $('.total').text(minPrice);
            } else if (value > maxStock) {
                alert("Value must has exceeded the stock");
                $('.nilai').val(maxStock);
                var maxPrice = maxStock * price;
                $('.total').text(maxPrice);
            }
        });

        var nilaiStr = $('.nilai').val();
        if (nilaiStr <= 1){
            $('.mines').attr('disabled', 'disabled');
            $('.js-submit-validation').removeAttr('disabled');
        }

        $('.plus').off().on('click',function(){
            var nilaiStr = $('.nilai').val();
            var nilai = parseInt(nilaiStr);
            $('.nilai').val(nilai+1);
            var newNilaiStr = $('.nilai').val();
            var newNilai = parseInt(newNilaiStr);
            var total = newNilai * price;
            $('.total').text(total);

            $('.mines').prop('disabled', false);
        });

        $('.mines').off().on('click',function(){
            var nilaiStr = $('.nilai').val();
            var nilai = parseInt(nilaiStr);
            $('.nilai').val(nilai-1);
            var newNilaiStr = $('.nilai').val();
            if (nilaiStr <= 2){
                $('.mines').prop('disabled', true);
            }

            var newNilai = parseInt(newNilaiStr);
            var total = newNilai * price;
            $('.total').text(total);
        });

        $('.productAdd').off().on('click',function(){
            var stock = parseInt($(parent).find('.js-tbl-product-stok').text());
            var idProduct = $(parent).find('.js-tbl-product-id').val();
            var product = $(modal).find('.js-product-name').text();
            var qwt     = parseInt($(modal).find('.nilai').val());
            var total   = $(modal).find('.total').text();
            var number = 0;

            var template = "<tr>\n" +
            "                            <td class=\"js-id-product\" data-id="+idProduct+">"+ idProduct +"</td>\n" +
            "                            <td class=\"js-produk\">"+product+"</td>\n" +
            "                            <td value=\"\" class=\"js-qwt\" data-stock="+idProduct+">"+qwt+"</td>\n" +
            "                            <td> Rp  <span class=\"js-price\">"+total+"</span></td>\n" +
            "                            <td><a><i><span class=\"fa fa-trash js-delete-data\"></span></i></a></td>\n" +
            "                        </tr>";

            var isNewProduct = true;
            var idExistingProduct;
            var data = {
                "id": idProduct,
                "stock" : qwt
            };

            code.push(data);
            idList.push(idProduct);
            quantities.push(qwt);

            var allIdProduct = $(".list-transaction").find(".js-id-product");
            $(".list-transaction").find(".js-id-product").each(function(){
                var _this = $(this);
               if (idProduct == _this.data("id")){
                   isNewProduct = false;
                   idExistingProduct = $(this).data("id");
                   return false;
               }
            });
            if (isNewProduct){
                $(".list-transaction").append(template);
            }
            else {
                var existingData = $(".list-transaction").find('.js-id-product[data-id="'+idProduct+'"]');
                var existingQty = existingData.siblings('.js-qwt').text();
                var existingTotal = existingData.siblings('td').find('.js-price').text();

                var newQty = parseInt(existingQty) + qwt;
                var newTotal = parseInt(existingTotal) + parseInt(total);

                existingData.siblings('.js-qwt').text(newQty);
                existingData.siblings('td').find('.js-price').text(newTotal);
            }
            countTotalPrice();

            var realStock = stock - qwt;
            $(parent).find('.js-tbl-product-stok').text(realStock);

            $('.js-table-product').show();
            $('#search').val("");
        });
        modal.find('.js-product-name').text(productName);
        modal.find('.js-product-price').text(price);
        modal.find('.js-product-image').attr("src", image);

    });

    $('.list-transaction').on('click', '.js-delete-data', function () {

        var _this = $(this);
        var removedProductId =  _this.closest('td').siblings('.js-id-product').text();
        var removedProductQwt = _this.closest('td').siblings('.js-qwt').text();

        var targetProduct = $('.js-table-product[data-id="'+removedProductId+'"]');
        var targetProductQwt = targetProduct.find('.js-tbl-product-stok').text();

        var newProductQwt = parseInt(targetProductQwt) + parseInt(removedProductQwt);
        targetProduct.find('.js-tbl-product-stok').text(newProductQwt);

        $(this).closest('tr').remove();

        countTotalPrice();
    });


    var countTotalPrice = function(){
        var totalPrice = $('.js-price');
        var total = 0;
        totalPrice.each(function () {
            var price = $(this).text();
            var number = price.replace('Rp',"");
            var totalPriceInt = parseInt(number);
            total += parseFloat(totalPriceInt);
        });
        $('.js-total-price').text(total);
    };


    $('.js-save-data').on('click', function(){
        var list    = $('.list-transaction');
        console.log(idList);

        var url = $(this).data('url');

        _executeListAction(url, $.param({
            ids : idList,
            quantities : quantities
        }, true)) ;

        if ( list < 1 || list.length < 1) {
            alert("No Transaction List. Make New Transaction!");
        }
        else{
            alert("Transaction Success!");
        }
        list.remove();
        totalPrice.remove();
    });

    var _executeListAction = function (url, params) {
        $.post(url, params, function () {
                location.reload();
        })
    }
    
    
    $('#search').on('input',function () {
        var textInput = $(this).val();
        $('.js-table-product').each( function () {
            var nameProduct = $(this).find('.js-tbl-product-name').text();
            var priceProduct = $(this).find('.js-tbl-product-price').text();
            console.log(priceProduct);
            if((textInput.toLowerCase() == nameProduct.toLowerCase()) || (textInput == priceProduct)){
                $(this).show();
            } else if (textInput == 0) {
                $('.js-table-product').show();
                $('#search').val("");
            }
            else {
                $(this).hide();
            }
        });

        // var input  = document.getElementById("search");
        // var filter = input.value.toUpperCase();
        // var table  = document.getElementsByClassName("js-table");
        // console.log("Input : ", input, ". Filter : ", filter, ". Tabel : " , table);
        // var nameProduct = table.getElementsByClassName("js-table-product");
        // var i;
        // for (i = 0; i < nameProduct.length ; i++){
        //     var product = nameProduct[i].getElementsByTagName("tr")[0];
        //     var txtValue = product.textContent || product.innerText;
        //     if (txtValue.toUpperCase().indexOf(filter) > -1){
        //         nameProduct[i].display= "";
        //     } else {
        //         nameProduct[i].display= "none";
        //     }
        // }
    });
})(jQuery);