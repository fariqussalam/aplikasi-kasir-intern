<%--
  Created by IntelliJ IDEA.
  User: fairtech
  Date: 2020-01-03
  Time: 19:08
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="backend"/>
</head>

<body>
<section>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <div class="dashboard">
                    <h1>DASHBOARD</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 left">
                <div class="leftSide">
                    <div class="row option">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="#"><i class="fa fa-tachometer mr-2"></i>Inventory
                                Management</a>
                                <hr class="bg-white">
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="${createLink(controller : 'product', action: 'index')}"><i class="fa fa-shield mr-2"></i>Product List
                                </a>
                                <hr class="bg-white">
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href=""><i class="fa fa-shield mr-2"></i>Transaction
                                </a>
                                <hr class="bg-white">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="rightSide">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="navigasi">
                                <ul class="breadcrumb">
                                    <li><a href="#">Inventory Management</a></li>
                                    <li><a href="#">Product List</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="addProduct">
                                <g:link type="button" class="btn btn-secondary" controller="Product" action="add"> ADD PRODUCT</g:link>
                            </div>
                        </div>
                    </div>
                    <div class="listProduct">
                        <div class="card">
                            <div class="table">
                                <table>
                                    <thead class="listProduct">
                                    <th class="no">No</th>
                                    <th >Id</th>
                                    <th class="info">Code</th>
                                    <th class="info">Date Transaction</th>
                                    <th class="info">Total Payment</th>
                                    <th colspan="2">Action</th>
                                    </thead>
                                    <tbody>
                                    <g:each in="${product}" var="pro" status="i">
                                        <tr>
                                            <td>${i+1}</td>
                                            <td><a href ="${createLink(controller: 'Transaction', action: 'detail', params: [transactionId: pro.id])}">${pro.id}</a></td>
                                            <td>${pro.code}</td>
                                            <td><g:formatDate date="${pro.dateTransaction}" format="dd MMM yyyy" /></td>
                                            <td><span>Rp. </span>${pro.totalPayment}</td>
                                            <td colspan="2">
                                                <g:link url="[controller:'transaction', action:'delete', id:pro.id]" class="fa fa-trash warn"></g:link>
                                            </td>
                                        </tr>
                                    </g:each>
                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <g:set var="max" value="${Math.min(params?.int('max',5), 7)}" />
                                <g:set var="page" value="${params?.int('page')?: 1}" />
                                <g:set var="totalCount" value="${transactionCount}"/>
                                <g:set var="totalPage" value="${Math.ceil(totalCount/max) as Integer}"/>
                                <nav aria-label="Page navigation example"></nav>
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="${createLink(controller: 'transaction', action: 'index', params: [page : page-1, max : max])}" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">${page}</a></li>
                                    <li class="page-item"><a class="page-link">of</a></li>
                                    <li class="page-item"><a class="page-link" href="#">${totalPage}</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="${createLink(controller: 'transaction', action: 'index', params: [page : page+1, max : max])}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row footer">
                        <div class="col-md-12">
                            <p>Copyright 2019 Cashier Intern. All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
%{--        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">--}%
%{--            <div class="modal-dialog" role="document">--}%
%{--                <div class="modal-content">--}%
%{--                    <div class="modal-header">--}%
%{--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}%
%{--                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>--}%
%{--                    </div>--}%
%{--                    <div class="modal-body">--}%
%{--                    </div>--}%
%{--                    <div class="modal-footer">--}%
%{--                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>--}%
%{--                        <button type="button" class="btn btn-primary">OK</button>--}%
%{--                    </div>--}%
%{--                </div>--}%
%{--            </div>--}%
%{--        </div>--}%
    </div>
</section>
</body>

</html>