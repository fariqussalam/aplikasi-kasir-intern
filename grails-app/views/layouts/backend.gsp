<%--
  Created by IntelliJ IDEA.
  User: fairtechoffice
  Date: 2019-12-18
  Time: 16:21
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Kasir"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="admin.css"/>

    <g:layoutHead/>
</head>

<body>

    <main>
        <g:layoutBody/>
    </main>

    <asset:javascript src="admin.js"/>

</body>
</html>