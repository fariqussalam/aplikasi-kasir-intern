<%--
  Created by IntelliJ IDEA.
  User: fairtechoffice
  Date: 2019-12-18
  Time: 14:57
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Kasir"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="application.css"/>



    <g:layoutHead/>
</head>

<body>
    <nav class="header">
        <div class="top">
            <h2>CASHIER</h2>
        </div>
    </nav>

    <main>
        <g:layoutBody/>
    </main>


    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p style="text-align:center;">Copyright 2019 Cashier Intern. All rights reserved</p>
                </div>
            </div>
        </div>
    </footer>
    <asset:javascript src="application.js"/>
</body>
</html>