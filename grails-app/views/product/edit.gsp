<%--
  Created by IntelliJ IDEA.
  User: fairtechoffice
  Date: 2019-12-16
  Time: 14:06
--%>

<%@ page import="aplikasi.kasir.intern.Product" contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html class="full-screen-mobile no-js" lang="en">

<head>
    <meta name="layout" content="backend"/>
</head>
<body>
<section>
    <div class="add">
        <div class="row">
            <div class="col-md-12">
                <div class="dashboard">
                    <h1>DASHBOARD</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 left">
                <div class="leftSide">
                    <div class="row option">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="#"><i class="fa fa-tachometer mr-2"></i>Inventory
                                Management</a>
                                <hr class="bg-white">
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-shield mr-2"></i>Product List
                                </a>
                                <hr class="bg-white">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="rightSide">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navigasi">
                                <ul class="breadcrumb">
                                    <li><a href="#">Inventory Management</a></li>
                                    <li><a href="#">Product List</a></li>
                                    <li><a href="#">Edit Product</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <form action="${createLink(controller: "Product", action: "update", id: params?.id)}" method="post" enctype="multipart/form-data">
%{--                        params: [id:product.id]--}%
                        <div class="wrapper">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="mt-3">
                                            <a>Name: </a><br>
                                            <input type="text" name="name" value="${edit.name}" />
                                        </div>
                                        <div class="mt-3">
                                            <a>Code: </a><br>
                                            <input type="text" name="code" value="${edit.code}" />
                                        </div>
                                        <div class="mt-3">
                                            <a>Price : </a><br>
                                            <input type="text" name="price" value="${edit.price}" />
                                        </div>
                                        <div class="mt-3">
                                            <a>Stock : </a><br>
                                            <input type="text" name="stock" value="${edit.stock}" />
                                        </div>
                                        <div class="mt-3">
                                            <a>Description : </a><br>
                                            <g:textArea type="text" name="description" value="${edit.description}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>Tumbnail Product</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="preview">
                                                <p> Preview Image</p>
                                                <img src="${resource(dir: 'assets/image', file: edit.thumbnailUrl)}" id="photo"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="file" id="upload" name="thumbnailUrl" /> <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row submit">
                                <div class="col-md-12">
                                    <button class="btn btn-dark">CANCEL</button>
                                    <button type="submit" class="btn btn-dark" >SAVE</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row footer">
                        <div class="col-md-12">
                            <p>Copyright 2019 Cashier Intern. All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

</html>