<%--
  Created by IntelliJ IDEA.
  User: fairtechoffice
  Date: 2019-12-16
  Time: 11:20
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html class="full-screen-mobile no-js" lang="en">

<head>
    <meta name="layout" content="backend"/>
</head>

<body>
<section>
    <div class="add">
        <div class="row">
            <div class="col-md-12">
                <div class="dashboard">
                    <h1>DASHBOARD</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 left">
                <div class="leftSide">
                    <div class="row option">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="#"><i class="fa fa-tachometer mr-2"></i>Inventory
                                Management</a>
                                <hr class="bg-white">
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-shield mr-2"></i>Product List
                                </a>
                                <hr class="bg-white">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="rightSide">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navigasi">
                                <ul class="breadcrumb">
                                    <li><a href="#">Inventory Management</a></li>
                                    <li><a href="#">Product List</a></li>
                                    <li><a href="#">Add Product</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <form action="${createLink(controller: "Product", action: 'save')}" class="js-form" method="post" enctype="multipart/form-data">
                    <div class="wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="mt-3">
                                        <a>Name: </a><br>
                                        <input type="text" Name="name"/>
                                    </div>
                                    <div class="mt-3">
                                        <a>Code: </a><br>
                                        <input type="text" Name="code" class="js-code-data" data-url="${createLink(controller: "product", action: "check")}"/>
                                        <span class="alert-danger" id="warningMessage"></span>
                                    </div>
                                    <div class="mt-3">
                                        <a>Price : </a><br>
                                        <input type="text" Name="price"/>
                                    </div>
                                    <div class="mt-3">
                                        <a>Stock : </a><br>
                                        <input type="text" Name="stock"/>
                                    </div>
                                    <div class="mt-3">
                                        <a>Description : </a><br>
                                        <textarea type="text" Name="description"/></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>Tumbnail Product</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="preview" style="height: 250px">
                                            <p> Preview Image</p>
                                            <img src="" id="photo"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="file" id="upload" name="thumbnailUrl"/> <br>
                                        <div class="btn btn-danger">
                                            <a href="${createLink(controller: "product", action: "add")}">REMOVE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row submit">
                            <div class="col-md-12">
                                <div class="btn btn-dark">
                                    <a href="${createLink(controller: "product", action: "index")}">CANCEL</a>
                                </div>
                                <button type="submit" class="btn btn-dark js-save-product">SAVE</button>
                            </div>
                        </div>
                    </div>
                    </form>
                    <div class="row footer">
                        <div class="col-md-12">
                            <p>Copyright 2019 Cashier Intern. All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

</html>