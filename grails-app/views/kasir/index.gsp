<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="frontend"/>
</head>
<body>
<section class="konten">
    <div class="row">
        <div class="col-md-7 margin">
            <h3>Product List </h3>
            <div class="card">
                <div class="list">
                    <div class="row">
                        <div class="col-md-4 search">
                            <div class="input-group mb-2">
%{--                                <form action="${createLink(controller: "kasir", action: "result")}"  method="get">--}%
                                <form >
                                    <div class="input-group-prepend">
                                        <input type="text" class="form-control" id="search" placeholder="Search">
%{--                                        <div class="input-group-text cari"><button type="submit"><span class="fa fa-search"></span></button></div>--}%
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <table class="js-table">
                        <thead class="tabel">
                        <th>#</th>
                        <th></th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Stock</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        <g:each in="${produk}" var="data" status="i">
                            <tr class="js-table-product" data-id="${data.id}">
                                <td>${i+1} <input class="js-tbl-product-id" type="hidden" value="${data.id}"> </td></td>
                                <td><img src="${resource(dir: 'assets/image', file: data.thumbnailUrl)}" class="image-wrapper js-product-image" data-image="${resource(dir: 'assets/image', file: data.thumbnailUrl)}" ></td>
                                <td class="js-tbl-product-name">${data.name}</td>
                                <td> Rp <span class="js-tbl-product-price">${data.price}</span></td>
                                <td class="js-tbl-product-stok" >${data.stock}</td>
                                <td><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-circle"></i></a></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
                <div style="margin-left: 450px">
                    <g:set var="max" value="${Math.min(params?.int('max',5), 7)}" />
                    <g:set var="page" value="${params?.int('page')?: 1}" />
                    <g:set var="totalCount" value="${produkcount}"/>
                    <g:set var="totalPage" value="${Math.ceil(totalCount/max) as Integer}"/>
                    <nav aria-label="Page navigation example"></nav>
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="${createLink(controller: 'kasir', action: 'index', params: [page : page-1, max : max])}" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">${page}</a></li>
                        <li class="page-item"><a class="page-link">of</a></li>
                        <li class="page-item"><a class="page-link" href="#">${totalPage}</a></li>
                        <li class="page-item">
                            <a class="page-link" href="${createLink(controller: 'kasir', action: 'index', params: [page : page+1, max : max])}" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="col-md-4 margin">
            <h3> Details Transaction</h3>
            <div class="card" id="resultDiv">
                <div class="details">
                    <table>
                        <tbody class="list-transaction">
                        <br>
                        </tbody>
                    </table>
                    <hr>
                    <div class="row buttom">
                        <div class="col-md-7">
                            <h3>Grand Total </h3>
                        </div>
                        <div class="col-md-5">
                            <h3> Rp <span class="js-total-price" >0 </span> </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <button class="btn btn-danger btn-lg button js-save-data" value="0" type="button" id="save" name="save" data-url="${createLink(controller: "kasir", action: "save")}">SUBMIT</button>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body" >
                    <input type="hidden" class="js-id-product" data-id=1>
                    <center>
                        <div>
                            <img class="image-wrapper js-product-image" style="width: 380px;">
                        </div>
                            <h2 class="js-product-name"></h2>
                            <h4> Rp <span class="js-product-price"> </span></h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <button style="border:none" class="mines" ><span class="fa fa-minus-circle" ></span></button>
                                </div>
                                <div class="col-md-4">
                                    <input type="number" class="nilai js-input-value" min="1" style="width: 50px; text-align: center;">
                                    <span class="alert-danger" id="warningMessage"></span>
                                </div>
                                <div class="col-md-4">
                                    <button style="border:none" class="plus"><span class="fa fa-plus-circle "></span></button>
                                </div>
                            </div>
                        <hr>
                        <h3> Rp <span class="total js-product-price"></span></h3>
                        <input type="button" value="SUBMIT" class="btn btn-danger btn-lg button productAdd js-submit-validation" data-dismiss="modal" style="background: #ED1C24">
                    </center>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>