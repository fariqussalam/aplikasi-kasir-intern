package aplikasi.kasir.intern

import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class ProductService {

    def getProductList(GrailsParameterMap params) {
        Integer max     = Math.min(params?.int("max",5),7)
        Integer page    = params?.int("page", 1)
        Integer offset  = (page - 1) * max

        def a = Product.createCriteria()
        def product = a.list(max: max, offset: offset) {
            order("id", "desc")
        }

        return product
    }
    Integer productCount(GrailsParameterMap params){
        int count = Product.count()
        return count
    }

    def getTransactionList(GrailsParameterMap params) {
        Integer max     = Math.min(params?.int("max",5),7)
        Integer page    = params?.int("page", 1)
        Integer offset  = (page - 1) * max

        def a = Transaction.createCriteria()
        def transaction = a.list(max: max, offset: offset) {
            order("id", "desc")
        }

        return transaction
    }
    Integer transactionCount(GrailsParameterMap params){
        int count = Transaction.count()
        return count
    }

    def saveTransaction(GrailsParameterMap params){
        List<Long> idList = params?.list("ids")*.toLong()
        List<Integer> quantities = params?.list("quantities")*.toInteger()

        List<Product> products = Product.findAllByIdInList(idList)
        BigDecimal totalpayment = 0
        Integer totalStock = 0

        idList?.eachWithIndex{ Long id, int idx ->
            Product product = Product.findById(id)
            if (product){
                Integer qty = quantities[idx]
                BigDecimal subTotal = product?.price * qty
                totalpayment = totalpayment + subTotal

                //suci
                Integer finalStock = product?.stock - qty
                totalStock = totalStock + finalStock
                product.stock = finalStock
                product.save()
            }
        }

        Transaction transaction = new Transaction()
        transaction.code = "qwerty"
        Date date = new Date()
        transaction.dateTransaction = date
        transaction.totalPayment = totalpayment

        processDetailTransaction(transaction, idList, quantities)
        transaction.save()

    }
    def processDetailTransaction(Transaction transaction, List<Long> idList, List<Integer> quantities){

        idList.eachWithIndex{ Long id, int idx ->
            DetailTransaction detailTransaction = new DetailTransaction()

            Product product = Product.findById(id)
            detailTransaction.product = product
            detailTransaction.jumlah = quantities[idx]

            transaction.addToDetailTransactions(detailTransaction)
        }
    }
}

