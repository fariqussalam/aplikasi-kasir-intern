package Intern

import aplikasi.kasir.intern.DetailTransaction
import aplikasi.kasir.intern.Product
import aplikasi.kasir.intern.Transaction

class TransactionController {
    def productService

    def index() {
//        def listTransaction     = Transaction.list()
        def listTransaction    = productService.getTransactionList(params)
        Integer transactionCount = productService.transactionCount(params)
        [
                product     : listTransaction,
                transactionCount : transactionCount
        ]
    }

    def delete(){
        def i = Transaction.get(params.'id')

        i.delete()
        redirect(controller:"Transaction", action: "index")
    }

    def detail() {

        Transaction transaction = Transaction.findById(params.transactionId as Long)
        Integer produkCount = productService.productCount(params)
        [
                detail     : transaction.detailTransactions,
                produkcount : produkCount
        ]
    }
}
