package aplikasi.kasir.intern

import grails.transaction.Transactional

class ProductController {
    def productService

    def index() {
        def listProduct     = productService.getProductList(params)
        Integer produkCount = productService.productCount(params)
        [
                produk      : listProduct,
                produkcount : produkCount
        ]
    }

    def add(){

    }

    def edit(){
        def a = Product.get(params.id)
        [
                edit : a
        ]
    }


    def save() {
        Date date = new Date()
        Product p = new Product()
//        Product x = Product.findByCode(params.code)

        p.code = params.code
        p.name = params.name
        p.price = params.price as BigDecimal
        p.stock = params.stock as Integer
        p.description = params.description
        p.dateCreated = date

        def file = request.getFile("thumbnailUrl")
        String imageUploadPath="/Users/fairtechoffice/Documents/Ciii/aplikasi-kasir-intern/aplikasi-kasir-intern/grails-app/assets/image"
        try{
            if(file && !file.empty){
                file.transferTo(new File("${imageUploadPath}/${file.filename}"))
                p.thumbnailUrl = "${file.filename}"
            }
        }
        catch(Exception e) {
            log.error("Your exception message goes here", e)
        }

//        if (p.code == x?.code){
//            def oldStock = p.stock as Integer
//            def newStock = x.stock as Integer
//            p.stock = oldStock + newStock
//        } else{
//            p.stock = params.stock as Integer
//        }

//        x.delete()
        p.save()
        redirect([controller: "Product", action: "index"])
    }

    @Transactional
    def update() {
        Date date = new Date()
        Product p = Product.findById(params.long("id"))
        p.code = params.code
        p.name = params.name
        p.price = params.price as BigDecimal
        p.stock = params.stock as Integer
        p.description = params.description
        p.dateCreated = date
//        p.thumbnailUrl = params.thumbnailUrl

        def file = request.getFile("thumbnailUrl")
        String imageUploadPath="/Users/fairtechoffice/Documents/Ciii/aplikasi-kasir-intern/aplikasi-kasir-intern/grails-app/assets/images"
        try{
            if(file && !file.empty){
                file.transferTo(new File("${imageUploadPath}/${file.filename}"))
                p.thumbnailUrl = "${file.filename}"
            }
        }
        catch(Exception e) {
            log.error("Your exception message goes here", e)
        }

        p.save()
        redirect(controller: "Product", action: "index")
    }

    def delete(){
        Product product = Product.findById(params.'id')

            if (product){
                product.listProduct?.findAll()?.each {
                    product.removeFromListProduct(it)
                    it.delete()
                }
            }
            product.delete()


//        def i = Product.get(params.'id')
//
//        i.delete()
        redirect(controller:"Product", action: "index")
    }

   def check(){
       Boolean isExist = false
       Product product = Product.findByCode(params.code)
       if (product){
           isExist = true
       }
       render isExist


   }
}

