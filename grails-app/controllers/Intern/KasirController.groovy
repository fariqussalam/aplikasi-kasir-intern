package aplikasi.kasir.intern

import groovy.sql.Sql

class KasirController {
    def productService

    def index() {
        def data     = productService.getProductList(params)
//        List<Product> data = Product.list()
        Integer produkCount = productService.productCount(params)
        [
                produk : data,
                produkcount : produkCount
        ]
    }
    def result(){
        String keyword = params.search as String
        List<Product> produk = Product.createCriteria().list {
            ilike("name","%" + keyword + "%")
        }
        render(view:"index", model: [produk: produk ])
    }
    def save(){
        def saveTransaction = productService.saveTransaction(params)
    }
}
